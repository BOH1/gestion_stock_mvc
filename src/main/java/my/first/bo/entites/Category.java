package my.first.bo.entites;
 import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

 
 
 
 @Entity
public class Category  implements Serializable{

	 @Id
	 @GeneratedValue
	 private Long idCategory;
	 
	 private String designation;
	 
	      private String code; 
	      
	      
	 public Category()
	 {
		 //TODO
		 
	 }
	
	public String getDesignation() {
			return designation;
		}

		public void setDesignation(String designation) {
			this.designation = designation;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public List getArticles() {
			return articles;
		}

		public void setArticles(List articles) {
			this.articles = articles;
		}

		@OneToMany(mappedBy = "category")
	private List<Article> articles;

	public Long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Long id) {
		this.idCategory = id;
	}
	 
	 
}
