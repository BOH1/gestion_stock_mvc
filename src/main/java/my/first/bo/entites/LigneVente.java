package my.first.bo.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LigneVente implements Serializable{
	
	 @Id
	 @GeneratedValue
	 private Long idLigneVente;

	 @ManyToOne
	 @JoinColumn(name = "idArticle")
	 private Article article;
	 
	 @ManyToOne
	 @JoinColumn(name = "vente")
	 private Vente vente;

	public Long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	 
 
	 
	 
}
